#include "Original.h"

String Original::face_cascade_name = "haarcascade_frontalface_alt.xml";
String Original::eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier Original::face_cascade;
CascadeClassifier Original::eyes_cascade;

Original::Original(Mat img)
{
	this->img = img;
}


void Original::detectAndDisplay()
{
  std::vector<Rect> faces;
  Mat frame_gray;

  cvtColor( img, frame_gray, CV_BGR2GRAY );
  equalizeHist( frame_gray, frame_gray );

  //-- Detect faces
  face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30) );

  for( size_t i = 0; i < faces.size(); i++ )
  {
    Point center( faces[i].x + faces[i].width*0.5, faces[i].y + faces[i].height*0.5 );
    ellipse( img, center, Size( faces[i].width*0.5, faces[i].height*0.5), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );

    Mat faceROI = frame_gray( faces[i] );
    std::vector<Rect> eyes;

    //-- In each face, detect eyes
    eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) );

    for( size_t j = 0; j < eyes.size(); j++ )
     {
       Point center( faces[i].x + eyes[j].x + eyes[j].width*0.5, faces[i].y + eyes[j].y + eyes[j].height*0.5 );
       int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
       circle( img, center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
     }
  }
  //-- Show what you got
  namedWindow("Original", WINDOW_NORMAL);
  imshow( "Original", img );
 }


void Original::loadClassifiers()
{
	if (!Original::face_cascade.load(face_cascade_name))
	{
		std::cerr << "Hiba az arcfelismero xml betoltese kozben!";
		exit(-1);
	};

	if (!Original::eyes_cascade.load(eyes_cascade_name))
	{
		std::cerr << "Hiba a szemfelismero xml betoltese kozben!";
		exit(-1);
	};
}