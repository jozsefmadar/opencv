#include "Arcfelismero.h"

String Arcfelismero::face_cascade_name = "haarcascade_frontalface_alt.xml";
String Arcfelismero::eyes_cascade_name = "haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier Arcfelismero::face_cascade;
CascadeClassifier Arcfelismero::eyes_cascade;

Arcfelismero::Arcfelismero(Mat img)
{
	this->img = img;
}

void Arcfelismero::detect()
{
	Mat img_gray;

	cvtColor(img, img_gray, COLOR_BGR2GRAY);
	equalizeHist(img_gray, img_gray);

	face_cascade.detectMultiScale(img_gray, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));

	for (size_t i = 0; i < faces.size(); i++)
	{
		Mat faceROI = img_gray(faces[i]);
		std::vector<Rect> eyes;

		eyes_cascade.detectMultiScale(faceROI, eyes, 1.1, 1, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
		this->eyes.push_back(eyes);
	}
}

Mat Arcfelismero::visualize()
{
	Mat ki = img.clone();
	for (size_t i = 0; i < faces.size(); i++)
	{
		Point center(faces[i].x + faces[i].width * 0.5, faces[i].y + faces[i].height * 0.5);
		Point sugar(faces[i].width * 0.5, faces[i].height * 0.5);
		if (sugar.x > 100 and sugar.y > 100)
			{
				ellipse(ki, center, sugar, 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
			}
		for (size_t j = 0; j < eyes[i].size(); j++)
		{
			Point center(faces[i].x + eyes[i][j].x + eyes[i][j].width * 0.5,
				faces[i].y + eyes[i][j].y + eyes[i][j].height * 0.5);
			int radius = cvRound((eyes[i][j].width + eyes[i][j].height) * 0.25);
			circle(ki, center, radius, Scalar(255, 0, 0), 4, 8, 0);
		}
	}

	return ki;
}

vector<Mat> Arcfelismero::getProfiles()
{
	vector<Mat> profiles;

	for (size_t i = 0; i < faces.size(); i++)
	{
		Mat ROI = img(faces[i]);
		profiles.push_back(ROI);
	}

	return profiles;
}

void Arcfelismero::loadClassifiers()
{
	if (!Arcfelismero::face_cascade.load(face_cascade_name))
	{
		std::cerr << "Hiba az arcfelismero xml betoltese kozben!";
		exit(-1);
	};

	if (!Arcfelismero::eyes_cascade.load(eyes_cascade_name))
	{
		std::cerr << "Hiba a szemfelismero xml betoltese kozben!";
		exit(-1);
	};
}