#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/core/core.hpp"


#include <iostream>

using namespace std;
using namespace cv;


/*!
Az arcfelismerésért felelős osztály.
*/
class Arcfelismero
{
	///Arcfelismerést segítõ xml állomány helye a fájlrendszerben
	static String face_cascade_name;

	///Szemfelismerést segítõ xml állomány helye a fájlrendszerben
	static String eyes_cascade_name;

	///Az xml alapján történõ arcfelismerést végrehajtó osztály
	static CascadeClassifier face_cascade;

	///Az xml alapján történõ szemfelismerést végrehajtó osztály
	static CascadeClassifier eyes_cascade;

	///Egy lista mely a képen található arcok helyzetét és méretét tárolja
	vector<Rect> faces;
	
	///Egy lista mely az egyes arcokhoz tartozó szemek helyzetét és méretét tárolja
	vector<vector<Rect> > eyes;

	///A kép mely köré a műveletek épülnek
	Mat img;

public:
	/*!
	Az osztály konstruktora, inicializálja az osztályt és betölti a képet.
	*/
	Arcfelismero(Mat img);

	/*!
	Elvégzi az arc és a szemek felismerését a konstruktorban megadott képen.
	*/
	void detect();

	/*!
	Visszaad egy képet, melyen a képen szereplõ emberek arcát és szemét bekarikázza.

	\return egy kép, melyen a képen szereplõ emberek arca és szeme be van karikázva
	*/
	Mat visualize();

	/*!
	Visszaad egy képekből álló listát, melyben a képen szereplő emberek arca látható.

	\return egy képekből álló lista, melyben a képen szereplő emberek arca látható
	*/
	vector<Mat> getProfiles();

	/*!
	A felismerést segítő xml állományok betöltéséért felelős.
	*/
	static void loadClassifiers();
};