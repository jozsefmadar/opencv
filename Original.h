#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/core/core.hpp"


#include <iostream>

using namespace std;
using namespace cv;

/*!
Az arcfelismerésért felelős osztály.
*/
class Original
{
	///Arcfelismerést segítõ xml állomány helye a fájlrendszerben
	static String face_cascade_name;

	///Szemfelismerést segítõ xml állomány helye a fájlrendszerben
	static String eyes_cascade_name;

	///Az xml alapján történõ arcfelismerést végrehajtó osztály
	static CascadeClassifier face_cascade;

	///Az xml alapján történõ szemfelismerést végrehajtó osztály
	static CascadeClassifier eyes_cascade;

	///Egy lista mely a képen található arcok helyzetét és méretét tárolja
	vector<Rect> faces;
	
	///Egy lista mely az egyes arcokhoz tartozó szemek helyzetét és méretét tárolja
	vector<vector<Rect> > eyes;

	///A kép mely köré a műveletek épülnek
	Mat img;

public:
	/*!
	Az osztály konstruktora, inicializálja az osztályt és betölti a képet.
	*/
	Original(Mat img);

	/*!
	Elvégzi az arc és a szemek felismerését a konstruktorban megadott képen.
	*/
	void detectAndDisplay();

	static void loadClassifiers();
};