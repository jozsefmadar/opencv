#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/core/core.hpp"
#include "Arcfelismero.h"
#include "Original.h"

#include <iostream>
#include <unistd.h>

using namespace std;
using namespace cv;

///A megjelenõ ablak neve
string window_name = "Arcfelismeres";


int main( int argc, const char** argv )
{
	VideoCapture capture(argc == 1 ? 0 : argv[1]);
	Mat frame;
	int frame_id = 0;
	
	Arcfelismero::loadClassifiers();
	Original::loadClassifiers();

	stringstream ss, cd;
	ss << "mkdir pillanatkep";
	system(ss.str().c_str());
	bool end = false;

	if (capture.isOpened())
	{
		while (capture.grab() && !end)
		{
			capture.read(frame);

			if (!frame.empty())
			{
				frame_id++;

				Arcfelismero arc(frame);
				arc.detect();
				Mat kimenet = arc.visualize();
				namedWindow(window_name, WINDOW_NORMAL);
				imshow(window_name, kimenet);

				Original ori(frame);
				ori.detectAndDisplay();
				int c = waitKey(10);

				switch (c)
				{
				case 'c':
					end = true;
					break;
				case 's':
					ss.str("");
					ss.clear();
					chdir("pillanatkep");
					ss << "pillanatkep-" << frame_id << ".jpg";
					imwrite(ss.str(), kimenet);

					cout << "Pillanatkep elmentve: " << ss.str() << endl;
					chdir("..");
					break;
				case 'p':
					vector<Mat> profiles = arc.getProfiles();

					ss.str("");
					ss.clear();
					ss << "mkdir profilkep";
					system(ss.str().c_str());
					chdir("profilkep");

					for (size_t i = 0; i < profiles.size(); i++)
					{
						ss.str("");
						ss.clear();
						ss << "profilkep-" << frame_id << i << ".jpg";
						imwrite(ss.str(), profiles[i]);

						cout << ss.str() << " kimentve" << endl;
					}

					cout << "-----------------------------------" << endl;
					cout << profiles.size() << " profilkep kimentve a pillanatkep\\" << frame_id << "\\-be" << endl;
					chdir("..");

					break;
				}
			}
			else
			{
				cout << " --(!) No captured frame -- Break!";
				break;
			}
		}
	}
	else
		cout<<"Error!"; cout<<endl;

	return 0;
}

